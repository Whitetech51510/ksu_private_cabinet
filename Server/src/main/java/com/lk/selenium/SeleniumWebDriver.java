package com.lk.selenium;
import com.lk.persistence.SeleniumUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

public class SeleniumWebDriver {
    private static Logger logger = LoggerFactory.getLogger(SeleniumWebDriver.class);
    public static void main1(String[] args)  throws  InterruptedException{
        WebDriver driver = null;
        logger.info("Start program.");
        try {
            String placeId = "place";
            String levelId = "level_select";
            String specialityId = "spec_select";
            String formOfEducationId = "form_select";
            String payId = "pay_select";
            Integer milliss = 100;
            Date dateStart = new Date();
            driver = SeleniumUtil.getDriver();
            driver.get("https://mai.ru/priem/rating");
            Thread.sleep(500);
            List<WebElement> placeOptions = new Select(driver.findElement(By.id(placeId))).getOptions();
            if(placeOptions.size()>0) {
                for (WebElement elemPlace : placeOptions) {
                    if(!elemPlace.getText().equals("---")){
                        driver.findElements(By.id(placeId)).get(0).click();
                        Thread.sleep(milliss);
                        elemPlace.click();
                        Thread.sleep(milliss);
                        List<WebElement> levelOptions = new Select(driver.findElement(By.id(levelId))).getOptions();
                        if (levelOptions.size() > 0){
                            for (WebElement level : levelOptions) {
                                if(!level.getText().equals("---")){
                                    driver.findElements(By.id(levelId)).get(0).click();
                                    Thread.sleep(milliss);
                                    level.click();
                                    Thread.sleep(milliss);
                                    List<WebElement> specOptions = new Select(driver.findElement(By.id(specialityId))).getOptions();
                                    if (levelOptions.size() > 0){
                                        for (WebElement spec : specOptions) {
                                            if(!spec.getText().equals("---")) {

                                                driver.findElements(By.id(specialityId)).get(0).click();
                                                Thread.sleep(milliss);
                                                spec.click();
                                                Thread.sleep(milliss);

                                                List<WebElement> formOptions = new Select(driver.findElement(By.id(formOfEducationId))).getOptions();
                                                if (formOptions.size() > 0) {
                                                    for (WebElement form : formOptions) {
                                                        if (!form.getText().equals("---")) {
                                                            driver.findElements(By.id(formOfEducationId)).get(0).click();
                                                            Thread.sleep(milliss);
                                                            form.click();
                                                            Thread.sleep(milliss);
                                                            List<WebElement> payOptions = new Select(driver.findElement(By.id(payId))).getOptions();
                                                            if (payOptions.size() > 0) {
                                                                for (WebElement pay : payOptions) {
                                                                    if (!pay.getText().equals("---")) {
                                                                        driver.findElements(By.id(payId)).get(0).click();
                                                                        Thread.sleep(milliss);
                                                                        pay.click();
                                                                        Thread.sleep(milliss);
                                                                        try {
                                                                            WebElement table = driver.findElement(By.xpath("//div[@id='tab']/table[@class='table']"));
                                                                            List<WebElement> rowsList = table.findElements(By.tagName("tr"));
                                                                            // List<WebElement> columnsList = null;
                                                                            for (WebElement row : rowsList) {
                                                                                logger.info(row.getText() + "\n\r");
                                                                            }
                                                                        } catch (Exception ex) {
                                                                            logger.error("No table found: ex message:" + ex.getLocalizedMessage());
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
            Date dateEnd = new Date();

            logger.info("End MAI parsing in "+(dateEnd.getTime() - dateStart.getTime())/1000+" sec");
            Thread.sleep(5000);

        } catch (Exception ex){
            logger.info("Exception:",ex.getLocalizedMessage(),ex);
        } finally {
            if(driver!=null) driver.quit();
        }
    }


    public static void main(String[] args)  throws  InterruptedException{
        WebDriver driver = null;
        logger.info("Start program. spbstu");
        try {
            String abiturientProgramId = "ab_vprogram";
            String formOfEducationId = "ab_edform";
            String payId = "ab_finance";
            String instituteId = "ab_departments";
            String groupId = "ab_groups";
            String buttonGet = "ab_common_items_button";
            String nullSeelct = "--- не выбрано ---";
            Integer milliss = 100;
            Date dateStart = new Date();
            driver = SeleniumUtil.getDriver();
            driver.get("https://www.spbstu.ru/abit/admission-campaign/general/abiturients/");
            Thread.sleep(500);
            List<WebElement> programOptions = new Select(driver.findElement(By.id(abiturientProgramId))).getOptions();
            if(programOptions.size()>0) {
                for (WebElement elemPlace : programOptions) {
                    if(!elemPlace.getText().equals(nullSeelct)){
                        driver.findElements(By.id(abiturientProgramId)).get(0).click();
                        Thread.sleep(milliss);
                        elemPlace.click();
                        Thread.sleep(milliss);
                        List<WebElement> formOptions = new Select(driver.findElement(By.id(formOfEducationId))).getOptions();
                        if (formOptions.size() > 0){
                            for (WebElement form : formOptions) {
                                if(!form.getText().equals(nullSeelct)){
                                    driver.findElements(By.id(formOfEducationId)).get(0).click();
                                    Thread.sleep(milliss);
                                    form.click();
                                    Thread.sleep(milliss);
                                    List<WebElement> payOptions = new Select(driver.findElement(By.id(payId))).getOptions();
                                    if (payOptions.size() > 0){
                                        for (WebElement pay : payOptions) {
                                            if(!pay.getText().equals(nullSeelct)) {

                                                driver.findElements(By.id(payId)).get(0).click();
                                                Thread.sleep(milliss);
                                                pay.click();
                                                Thread.sleep(milliss);

                                                List<WebElement> instituteOptions = new Select(driver.findElement(By.id(instituteId))).getOptions();
                                                if (formOptions.size() > 0) {
                                                    for (WebElement inst : instituteOptions) {
                                                        if (!inst.getText().equals(nullSeelct)) {
                                                            driver.findElements(By.id(instituteId)).get(0).click();
                                                            Thread.sleep(milliss);
                                                            inst.click();
                                                            Thread.sleep(200);
                                                            List<WebElement> groupOptions = new Select(driver.findElement(By.id(groupId))).getOptions();
                                                            if (payOptions.size() > 0) {
                                                                for (WebElement group : groupOptions) {
                                                                    if (group.getText().indexOf("Групп с такими параметрами нет.") < 0) {
                                                                        logger.info("Element:Place:"+elemPlace.getText()+". Form:"+form.getText()+". Pay:"+pay.getText()+".Institute:"+inst.getText()+". group:"+group.getText());
                                                                        try {
                                                                            driver.findElement(By.xpath("//select[@id='ab_groups'']/option[@value!='0']")); // checkk elem

                                                                            driver.findElements(By.id(groupId)).get(0).click();
                                                                            Thread.sleep(milliss);
                                                                            group.click();
                                                                            Thread.sleep(200);
                                                                            try {
                                                                                driver.findElements(By.id(buttonGet)).get(0).click();
                                                                                //todo
                                                                                //WebElement table = driver.findElement(By.xpath("//div[@id='tab']/table[@class='table']"));
                                                                                //List<WebElement> rowsList = table.findElements(By.tagName("tr"));
                                                                                //// List<WebElement> columnsList = null;
                                                                                //for (WebElement row : rowsList) {
                                                                                //    logger.info(row.getText() + "\n\r");
                                                                                //}
                                                                            } catch (Exception ex) {
                                                                                logger.error("No table found: ex message:" + ex.getLocalizedMessage());
                                                                            }
                                                                        }catch (Exception ex){
                                                                            logger.info("OPTIONS NOT FOUND ");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
            Date dateEnd = new Date();

            logger.info("End spbstu parsing in "+(dateEnd.getTime() - dateStart.getTime())/1000+" sec");
            Thread.sleep(5000);

        } catch (Exception ex){
            logger.info("Exception:",ex.getLocalizedMessage(),ex);
        } finally {
            if(driver!=null) driver.quit();
        }
    }

}
