package com.lk.impl;

import com.lk.entity.*;
import com.lk.persistence.HibernateUtil;
import com.lk.service.OtherUniversityReceiptService;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.apache.xpath.operations.Bool;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Named("otherUniversityReceipt")
public class OtherUniversityReceiptServiceImpl implements OtherUniversityReceiptService {

    private static Logger logger = LoggerFactory.getLogger(OtherUniversityReceiptServiceImpl.class);

    @Override
    public Response getOtherUniversityReceiptByUserId(Integer userId, Integer year) {
        List<LichnoeDelo> lichnoeDelo = new ArrayList<>();
        logger.info("Start function getOtherUniversityReceiptByUserId, by userId: " + userId);
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            lichnoeDelo = session.createSQLQuery("Select * from [LichnoeDelo] as ours where ours.IDLichnogoDela = (:UserId)")
                    .addEntity(LichnoeDelo.class)
                    .setParameter("UserId",userId)
                    .list();
            transaction.commit();
        } catch (Exception ex){
            logger.error("Exception in getUserByToken with:",ex.getLocalizedMessage(),ex);
            if (transaction!=null) transaction.rollback();
            return new Response(false, "Ошибка при получении статуса:"+ex.getLocalizedMessage());
        } finally {
            session.close();
        }
        LichnoeDelo user = lichnoeDelo.get(0);
        String patronymic = user.getOtchestvo();
        String fullName = user.getFamiliya() + " " + user.getImya();
        if(patronymic!=null && patronymic!="") fullName += " " + patronymic;
        List<OtherUniversityReceipt> otherUniversityReceipts = searchForUniversity(fullName,year);
        return new Response(true, otherUniversityReceipts);
    }

    @Override
    public Response getOtherUniversityReceiptByName(UniversityReceiptFilter universityReceiptFilter)  {
        String fullName = universityReceiptFilter.getFullName();
        Integer year = universityReceiptFilter.getYear();
        logger.info("Start function getOtherUniversityReceiptByName, by fullName: " + fullName);
        List<OtherUniversityReceipt> otherUniversityReceipts = searchForUniversity(fullName,year);
        return new Response(true, otherUniversityReceipts);
    }

   /* public List<OtherUniversityReceipt> searchForUniversity(String fullName, Integer year){
        List<OtherUniversityReceipt> otherUniversityReceipts = searchAbiturientInOtherUniversities(fullName,year);
        if(otherUniversityReceipts.size()>0) return otherUniversityReceipts;
        else {
            //to do search in new docs
            return otherUniversityReceipts;
        }
    }*/

    /**
     * @param fullName Полное имя участника
     * @param year Год участия в конкурсе
     * @return  Лист рейтинговых списков (несколько специальностей)
     * */
    public  List<OtherUniversityReceipt> searchForUniversity(String fullName, Integer year){
        List<OtherUniversityReceipt> otherUniversityReceipts = new ArrayList<>();
        logger.info("Start searchAbiturientInOtherUniversities for fullName: "+fullName);
        try {
            List<UniversityDigest> universityDigests = getUniversityDigestsByYear(year);

            for(UniversityDigest universityDigest : universityDigests) {
                String universityDigestUrlUrl = universityDigest.getUrl();
                URL url = new URL(universityDigestUrlUrl);
                InputStream inputStream = url.openStream();
                //String fileName = FilenameUtils.getName(url.getPath());
                String[] lines = parseDocument(inputStream);
                if (lines != null) {
                    for (String line : lines) {
                        if (line.toLowerCase().indexOf(fullName.toLowerCase()) >= 0) {
                            logger.info("Found item in function :searchAbiturientInOtherUniversities, Save this Indexing line:"+line);
                            OtherUniversityReceipt otherUniversityReceipt = new OtherUniversityReceipt();
                            otherUniversityReceipt.setUniversityName(universityDigest.getUniversityName());
                            otherUniversityReceipt.setUserFullName(fullName);
                            otherUniversityReceipt.setYear(year);
                            otherUniversityReceipt.setInformationAboutReceipt("Информация: "+universityDigest.getSpecialityName());
                            otherUniversityReceipts.add(otherUniversityReceipt);
                        }
                    }
                }
            }
        } catch (Exception ex){
            logger.error("Can't get searchAbiturientInOtherUniversities with exception:"+ex.getLocalizedMessage(), ex);
        }
        return otherUniversityReceipts;
    }

    public static void main(String [] args){
        ivanovoUniversityParseDocumentSchedule();
    }

    public static List<OtherUniversityReceipt> ivanovoUniversityParseDocumentSchedule(){
        logger.info("Entering into ivanovoUniversityParseDocumentSchedule function.");
        List<OtherUniversityReceipt> otherUniversityReceipts = new ArrayList<>();

        String universityDigestName = "Ивановский государственный университет";
        try {
            List<UniversityDigest> universityDigests = getDigestUnviersityByName(universityDigestName);
            if(universityDigests.size()>0) {
                String formOfEducation = "Очная";
                String payForm = "Бюджет";
                String educationName = "Бакалавар";
                String specialityName = "";
                String abiturientString = "";
                logger.info("Start university digest with size of digests items:"+universityDigests.size());
                for(UniversityDigest universityDigest:universityDigests){
                    Integer year = universityDigest.getYear();
                    String urlDigest = universityDigest.getUrl();
                    if(urlDigest== null || urlDigest =="" || year == null) {
                        logger.error("Year or url is null!");
                        break;
                    }
                    String universityName = "ФГБОУ ВО «Ивановский государственный университет»";
                    URL url = new URL(urlDigest);
                    InputStream inputStream = url.openStream();
                    if (inputStream != null) {
                        String[] lines = parseDocument(inputStream);
                        inputStream.close();
                        if (lines != null) {
                            Pattern pattern = Pattern.compile("[А-Яа-я]* [А-Яа-я]* [А-Яа-я]* ");
                            for (String line : lines) {
                                if(line.toLowerCase().indexOf("форма обучения")>=0) {
                                    formOfEducation = line;
                                }
                                if(line.toLowerCase().indexOf("форма финансирования")>=0) {
                                    payForm = line;
                                }
                                if(line.toLowerCase().indexOf("уровень образования")>=0) {
                                    educationName = line;
                                }
                                if(line.matches("\\d\\d\\.\\d\\d\\.\\d\\d .*")) {
                                    specialityName = line;
                                }
                                if(pattern.matcher(line).lookingAt()){
                                    if( line.split(" ").length > 5){
                                        abiturientString = line;
                                    }
                                }
                                if(abiturientString!="") {
                                    Boolean isOriginalDocument = false;
                                    String[] abitueintParams = abiturientString.split(" ");
                                    String abiturientFullName = abitueintParams[0] + " " + abitueintParams[1] + " " + abitueintParams[2] + " ";
                                    if(abiturientString.toLowerCase().indexOf("копия")>=0) isOriginalDocument = false;
                                    if(abiturientString.toLowerCase().indexOf("оригинал")>=0) isOriginalDocument = true;
                                    String information = formOfEducation + ", " + educationName + ", " + payForm + ", " + educationName + ", " + specialityName;
                                    AbiturientsReceipt abiturientsReceipt = new AbiturientsReceipt(null, universityName,abiturientFullName,information, year, null, null, isOriginalDocument,false );
                                    saveAbiturientReceipt(abiturientsReceipt);
                                    abiturientString = "";
                                    logger.info("Saved:" + formOfEducation +" "+ payForm  +" "+ educationName  +" "+ specialityName  +" "+ abiturientString);
                                }
                            }
                        } else {
                            logger.error("Document is null ar searchForIvanovoUniversity");
                        }
                    }

                }
            } else {
                logger.error("There is no university Digest items by universityName:"+universityDigestName);
            }
        } catch (Exception ex){
            logger.error("Can't get searchForIvanovoUniversity  with exception:"+ex.getLocalizedMessage(), ex);
        }
        return otherUniversityReceipts;
    }

    public static String[] parseDocument(InputStream inputStream){
        try {
            PDDocument document = PDDocument.load(inputStream);
            if (!document.isEncrypted()) {
                //PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                //stripper.setSortByPosition(true);
                PDFTextStripper tStripper = new PDFTextStripper();
                String pdfFileInText = tStripper.getText(document);
                document.close();
                // split by whitespace
                return pdfFileInText.split("\\r?\\n");
            }
            document.close();
        } catch (Exception ex){
            logger.error("Can't parse doc with exception:"+ex.getLocalizedMessage());
        }
        return null;
    }


    public static void saveAbiturientReceipt(AbiturientsReceipt abiturientsReceipt){
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            session.save(abiturientsReceipt);
            transaction.commit();
        } catch (Exception ex){
            if(transaction!=null) transaction.rollback();
            logger.error("Exception in saveAbiturientReceipt:",ex.getLocalizedMessage(),ex);
        } finally {
            session.close();
        }
    }

    public static List<UniversityDigest> getDigestUnviersityByName(String universityName){
        List<UniversityDigest> universityDigests = new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            universityDigests = session.createSQLQuery("SELECT * " +
                    "FROM ksu_abiturient.university_digest where universityName like '%"+universityName+"%'")
                    .addEntity(UniversityDigest.class)
                    .list();
            transaction.commit();
        } catch (Exception ex){
            logger.error("Exception in getUniversityDigestsByYear with:",ex.getLocalizedMessage(),ex);
            if (transaction!=null) transaction.rollback();
        } finally {
            session.close();
        }
        return universityDigests;
    }

    public List<UniversityDigest> getUniversityDigestsByYear(Integer year){
        List<UniversityDigest> universityDigests = new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            universityDigests = session.createSQLQuery("Select * from university_digest as digest where digest.Year = (:ThisYear)" +
                    "where !digest.isDeleted")
                    .addEntity(UniversityDigest.class)
                    .setParameter("ThisYear",year)
                    .list();
            transaction.commit();
        } catch (Exception ex){
            logger.error("Exception in getUniversityDigestsByYear with:",ex.getLocalizedMessage(),ex);
            if (transaction!=null) transaction.rollback();
        } finally {
            session.close();
        }
        return universityDigests;
    }

}
