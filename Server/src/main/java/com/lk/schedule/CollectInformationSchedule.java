package com.lk.schedule;

import com.lk.persistence.SeleniumUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


@Component
public class CollectInformationSchedule {

    private static Logger logger = LoggerFactory.getLogger(CollectInformationSchedule.class);


    @Scheduled(cron = "0 0 1 * * *") //Каждый день с понедельника по пятницу в 1 утра
    public void scheduled()   {
        seleniumCollect();
    }

    public void seleniumCollect(){
        mai();

    }

    public void mai(){
        WebDriver driver = null;
        try {
            driver = SeleniumUtil.getDriver();
            driver.get("https://mai.ru/priem/rating");
            String Id = "p20191227202911";
            for (int i = 1; i <= 5; i++) {
                String concatId = Id + "_" + i;

                Select select = new Select(driver.findElement(By.id("place")));
                select.selectByVisibleText("МАИ");
                Thread.sleep(300);
                Select select1 = new Select(driver.findElement(By.id("level_select")));
                select1.selectByValue("p20191227202911_1_l1"); // p20191227202911_1_l1_s66
                Thread.sleep(300);
                Select select2 = new Select(driver.findElement(By.id("spec_select")));
                select2.selectByValue("p20191227202911_1_l1_s1");
                Thread.sleep(300);
                Select select3 = new Select(driver.findElement(By.id("form_select")));
                select3.selectByVisibleText("очная");
                Thread.sleep(300);
                Select select4 = new Select(driver.findElement(By.id("pay_select")));
                select4.selectByVisibleText("Бюджет");
                Thread.sleep(500);
                WebElement table = driver.findElement(By.xpath("//table[@class='table']"));
                List<WebElement> rowsList = table.findElements(By.tagName("tr"));
                List<WebElement> columnsList = null;
                for (WebElement row : rowsList) {
                    System.out.println(row.getText() + "\n\r");
                }
                Thread.sleep(5000);
            }

        } catch (Exception ex){
            System.out.println(ex);
        } finally {
            if(driver!=null) driver.quit();
        }
    }

}
