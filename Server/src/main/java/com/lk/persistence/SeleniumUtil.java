package com.lk.persistence;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumUtil {

    final static String PathToDriver = "C:\\Users\\dsmirnov\\Desktop\\chromedriver.exe";
    static WebDriver webDriver = null;

    public static void setDriver(){
        System.setProperty("webdriver.chrome.driver", PathToDriver);
    }

    public static WebDriver getDriver(){
        setDriver();
        if(webDriver == null) webDriver = new ChromeDriver();
        return webDriver;
    }


}
