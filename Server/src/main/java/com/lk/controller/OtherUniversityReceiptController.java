package com.lk.controller;

import com.lk.entity.*;
import com.lk.service.OtherUniversityReceiptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.util.List;

@RestController
@RequestMapping("/OtherUniversity")
@CrossOrigin(origins = "*", maxAge = 3600)
public class OtherUniversityReceiptController {

    private OtherUniversityReceiptService otherUniversityReceipt;
    private Logger logger = LoggerFactory.getLogger(OtherUniversityReceiptController.class);

    @Inject
    public OtherUniversityReceiptController(@Named("otherUniversityReceipt") OtherUniversityReceiptService otherUniversityReceipt) {
        this.otherUniversityReceipt = otherUniversityReceipt;
    }

    @RequestMapping(value = "/getOtherUniversityReceiptByUserId/{userId}/{year}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Response getOtherUniversityReceiptByUserId (@PathVariable("userId") Integer userId,@PathVariable("year") Integer year, final HttpServletRequest request) {
        return otherUniversityReceipt.getOtherUniversityReceiptByUserId(userId,year);
    }


    @RequestMapping(value = "/getOtherUniversityReceiptByName", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public Response getOtherUniversityReceiptByName (@RequestBody UniversityReceiptFilter universityReceiptFilter) {
        return otherUniversityReceipt.getOtherUniversityReceiptByName(universityReceiptFilter);
    }


}
