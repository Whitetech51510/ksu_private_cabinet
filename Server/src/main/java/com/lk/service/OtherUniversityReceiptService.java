package com.lk.service;


import com.lk.entity.*;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
@RequestMapping("/OtherUniversityReceipt")
@CrossOrigin(origins = "*", maxAge = 3600)
public interface OtherUniversityReceiptService {

    public Response getOtherUniversityReceiptByUserId(Integer userId, Integer year);

    public Response getOtherUniversityReceiptByName(UniversityReceiptFilter universityReceiptFilter) ;

}
