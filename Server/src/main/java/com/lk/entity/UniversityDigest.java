package com.lk.entity;

public class UniversityDigest {

    private Integer Id;
    private String Url;
    private String UniversityName;
    private String SpecialityName;
    private Integer Year;
    private Boolean IsDeleted;

    public UniversityDigest(){}

    public UniversityDigest(String Url, String UniversityName, String SpecialityName, Integer Year, Boolean IsDeleted){
        this.Url = Url;
        this.UniversityName = UniversityName;
        this.SpecialityName = SpecialityName;
        this.Year = Year;
        this.IsDeleted = IsDeleted;
    }

    public Integer getId() { return Id; }
    public void setId(Integer id) { Id = id; }
    public Boolean getIsDeleted() { return IsDeleted; }
    public void setIsDeleted(Boolean deleted) { IsDeleted = deleted; }
    public String getUrl() { return Url; }
    public void setUrl(String url) { Url = url; }
    public String getUniversityName() { return UniversityName; }
    public void setUniversityName(String universityName) { UniversityName = universityName; }
    public String getSpecialityName() { return SpecialityName; }
    public void setSpecialityName(String specialityName) { SpecialityName = specialityName; }
    public Integer getYear() { return Year; }
    public void setYear(Integer year) { Year = year; }
}
