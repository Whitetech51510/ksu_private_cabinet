package com.lk.entity;

public class AbiturientsReceipt {

    private Integer Id;
    private String UniversityName;
    private String AbiturientFullName;
    private String InformationAboutReceipt;
    private Integer Year;
    private Integer NumberInList;
    private Integer ExamPoints;
    private Boolean IsOriginalDocument;
    private Boolean IsDeleted;

    public AbiturientsReceipt(){

    }

    public AbiturientsReceipt( Integer Id, String UniversityName, String AbiturientFullName, String InformationAboutReceipt, Integer Year, Integer NumberInList, Integer ExamPoints, Boolean IsOriginalDocument, Boolean IsDeleted){
        this. Id = Id;
        this.UniversityName = UniversityName;
        this.AbiturientFullName = AbiturientFullName;
        this.InformationAboutReceipt = InformationAboutReceipt;
        this. Year = Year;
        this. NumberInList = NumberInList;
        this. ExamPoints = ExamPoints;
        this. IsOriginalDocument = IsOriginalDocument;
        this. IsDeleted = IsDeleted;
    }

    public Integer getId() { return Id; }
    public void setId(Integer id) { Id = id; }
    public Boolean getIsDeleted() { return IsDeleted; }
    public void setIsDeleted(Boolean deleted) { IsDeleted = deleted; }
    public Integer getYear() { return Year; }
    public void setYear(Integer year) { Year = year; }
    public String getUniversityName() { return UniversityName; }
    public void setUniversityName(String universityName) { UniversityName = universityName; }
    public String getAbiturientFullName() { return AbiturientFullName; }
    public void setAbiturientFullName(String abiturientFullName) { AbiturientFullName = abiturientFullName; }
    public String getInformationAboutReceipt() { return InformationAboutReceipt; }
    public void setInformationAboutReceipt(String informationAboutReceipt) { InformationAboutReceipt = informationAboutReceipt; }
    public Integer getNumberInList() { return NumberInList; }
    public void setNumberInList(Integer numberInList) { NumberInList = numberInList; }
    public Integer getExamPoints() { return ExamPoints; }
    public void setExamPoints(Integer examPoints) { ExamPoints = examPoints; }
    public Boolean getIsOriginalDocument() { return IsOriginalDocument; }
    public void setIsOriginalDocument(Boolean originalDocument) { IsOriginalDocument = originalDocument; }
}
