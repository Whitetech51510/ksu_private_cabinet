package com.lk.entity;

import com.sun.org.apache.xpath.internal.operations.Bool;

public class Universities {

    private Integer Id;
    private String UniversityName;
    private Boolean IsDeleted;

    public Integer getId() { return Id; }
    public void setId(Integer id) { Id = id; }
    public String getUniversityName() { return UniversityName; }
    public void setUniversityName(String universityName) { UniversityName = universityName; }
    public Boolean getIsDeleted() { return IsDeleted; }
    public void setIsDeleted(Boolean deleted) { IsDeleted = deleted; }

}
