import com.lk.entity.UniversityDigest;
import com.lk.persistence.HibernateUtil;
import org.hibernate.Session;

public class migrationTest {

    public static void main1(String [] args){
        String[] urls  = new String[]{"http://www.ivanovo.ac.ru/upload/DocPriem2019/applications/bak_ochno_budget.pdf",
                "http://www.ivanovo.ac.ru/upload/DocPriem2019/applications/bak_ochno-zaochno_budget.pdf",
                "http://www.ivanovo.ac.ru/upload/DocPriem2019/applications/bak_zaochno_budget.pdf",
                "http://www.ivanovo.ac.ru/upload/DocPriem2019/applications/bak_ochno_kvota.pdf",
                "http://www.ivanovo.ac.ru/upload/DocPriem2019/applications/bak_ochno-zaochno_kvota.pdf",
                "http://www.ivanovo.ac.ru/upload/DocPriem2019/applications/bak_zaochno_kvota.pdf",
                "http://www.ivanovo.ac.ru/upload/DocPriem2019/applications/bak_ochno_celev.pdf",
                "http://www.ivanovo.ac.ru/upload/DocPriem2019/applications/bak_ochno_commerce.pdf",
                "http://www.ivanovo.ac.ru/upload/DocPriem2019/applications/bak_zaochno_commerce.pdf",
                "http://www.ivanovo.ac.ru/upload/DocPriem2019/applications/mag_ochno_commerce.pdf",
                "http://www.ivanovo.ac.ru/upload/DocPriem2019/applications/mag_ochno-zaochno_commerce.pdf",
                "http://www.ivanovo.ac.ru/upload/DocPriem2019/applications/mag_zaochno_commerce.pdf"};
        String univer = "Ивановский государственный университет";
        Integer year = 2019;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        for(int i = 0; i < urls.length; i++) {
            String url = urls[i];
            UniversityDigest universityDigest = new UniversityDigest(url,univer,null,year, false);
            session.save(universityDigest);

        }
        session.getTransaction().commit();
        session.close();
    }

    public static void main(String [] args){
        String[] urls  = new String[]{

        };
        String univer = "Ярославский государственный университет им. П.Г. Демидова";
        Integer year = 2019;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        for(int i = 0; i < urls.length; i++) {
            String url = urls[i];
            UniversityDigest universityDigest = new UniversityDigest(url,univer,null,year, false);
            session.save(universityDigest);

        }
        session.getTransaction().commit();
        session.close();
    }
}
