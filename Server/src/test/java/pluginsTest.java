import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import sun.util.calendar.BaseCalendar;
import java.util.Date;


import java.io.*;
import java.net.URL;
import java.util.List;

public class pluginsTest {



    public static void main(String[] args) throws IOException {
        WebDriver driver = null;
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\dsmirnov\\Desktop\\chromedriver.exe");
        try {
            driver = new ChromeDriver();
            driver.get("https://mai.ru/priem/rating/");
            Select select = new Select(driver.findElement(By.id("place")));
            select.selectByVisibleText("МАИ");
            Thread.sleep(300);
            Select select1 = new Select(driver.findElement(By.id("level_select")));
            select1.selectByValue("p20191227202911_1_l1"); // p20191227202911_1_l1_s66
            Thread.sleep(300);
            Select select2 = new Select(driver.findElement(By.id("spec_select")));
            select2.selectByValue("p20191227202911_1_l1_s1");
            Thread.sleep(300);
            Select select3 = new Select(driver.findElement(By.id("form_select")));
            select3.selectByVisibleText("очная");
            Thread.sleep(300);
            Select select4 = new Select(driver.findElement(By.id("pay_select")));
            select4.selectByVisibleText("Бюджет");
            Thread.sleep(500);
            WebElement table = driver.findElement(By.xpath("//table[@class='table']"));
            List<WebElement> rowsList = table.findElements(By.tagName("tr"));
            List<WebElement> columnsList = null;
            for (WebElement row : rowsList) {
                System.out.print(row.getText() + ", ");
                //columnsList = row.findElements(By.tagName("td"));
                //for (WebElement column : columnsList) {
                //    System.out.print(column.getText() + ", ");
                //}
            }
            Thread.sleep(5000);
        } catch (Exception ex){
            System.out.print("Ex:"+ex.getLocalizedMessage());
        }
    }

    public static void main1(String[] args) throws IOException {
        File file = new File("C:\\Users\\dsmirnov\\Desktop\\1.pdf");
        InputStream inputStream1 = new FileInputStream(file);
       // String file = "https://www.uniyar.ac.ru/Abitur/abiturientu-2018/reytingovye-spiski-magistratura-2018/02.04.02.pdf";
        //String file = "https://mapbasic.ru/doc/MapBasicUserGuide-7-0.pdf";
       // String file = "https://www.uniyar.ac.ru/Abitur/abiturientu-2018/reytingovye-spiski-magistratura-2018/01.04.02m.pdf";
      //  URL url = new URL(file);
       // InputStream inputStream = url.openStream();
        Date start = new Date();
        String[] pdfBox = parseDocumentAsPdfBox(file);
        Date end = new Date();
        System.out.println("PDF BOX time (milisecounds):"+(end.getTime() - start.getTime()));
        for(String text: pdfBox)
            System.out.println(text);
        Date start1 = new Date();
        String[] Itext = parseDocumentAsIText(file);
        Date end1 = new Date();
        System.out.println("ITEXT time (milisecounds):"+(end1.getTime() - start1.getTime()));
         for(String text: Itext)
             System.out.println(text);

    }

    public static  String[] parseDocumentAsPdfBox(File file){
        try {
            PDDocument documentParse = PDDocument.load(file);
            if (!documentParse.isEncrypted()) {
                PDFTextStripper stripper = new PDFTextStripper();
                String pdf = stripper.getText(documentParse);
                documentParse.close();
                return pdf.split("\\r?\\n");
            }
            documentParse.close();
        } catch (Exception ex){
            System.out.println("Ex:"+ex.getLocalizedMessage());
        }
        return null;
    }

    public static String[] parseDocumentAsIText(File file){
        try {

            PdfReader reader = new PdfReader( FileUtils.readFileToByteArray(file));
            String textFromPage = "";
            for(int i = 1 ; i<= reader.getNumberOfPages(); i++){
               textFromPage += PdfTextExtractor.getTextFromPage(reader,i);
            }
            return textFromPage.split("\\r?\\n");
        } catch (Exception  ex) {
            System.out.println("Ex:"+ex.getLocalizedMessage());
        }
        return null;
    }


}
